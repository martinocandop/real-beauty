import { RealBeautyPage } from './app.po';

describe('real-beauty App', function() {
  let page: RealBeautyPage;

  beforeEach(() => {
    page = new RealBeautyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
