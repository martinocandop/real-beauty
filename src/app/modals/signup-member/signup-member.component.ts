import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AFService } from '../../shared/services/firebase'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Router } from '@angular/router'

@Component({
  selector: 'app-signup-member',
  templateUrl: './signup-member.component.html',
  styleUrls: ['./signup-member.component.css']
})
export class SignupMemberComponent {
  public submited: boolean;
  public form: FormGroup;
  constructor(public af: AFService, public activeModal: NgbActiveModal, public fb: FormBuilder, private router: Router) {
    this.submited = false;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
      username: [null, Validators.minLength(5)],
      passwords: fb.group({
        password: [null, Validators.compose([Validators.required, Validators.minLength(7)])],
        confirm: [null, Validators.compose([Validators.required, Validators.minLength(7)])]
      }, { validator: this.areEqual })

    })
  }

  public register(form: any, event: Event) {
    this.submited = true;
    event.preventDefault();
    if (form.valid) {
      const user = { email: form.value.email, username: form.value.username, password: form.value.passwords.password }
      this.af.registerUser(user.email, user.password, user.username, "member").then(data => {
        alert(data)
        this.activeModal.close()
      }).catch(err => {
        alert(err)
      })
    }

  }

  public goSubscription() {
    this.activeModal.close();
    this.router.navigate(['/subscription']);

  }
  private areEqual(group: any) {
    const password = group.controls.password.value
    const confirm = group.controls.confirm.value
    let areEqual: boolean;
    if (password == confirm)
      return null
    return {
      areEqual: false
    };
  }
}
