import { Component } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AFService } from '../../shared/services/firebase'
import { UserService } from '../../shared/services/utils'
import { SignupMemberComponent } from '../signup-member/signup-member.component';
import { RecoverPasswordComponent } from '../recover-password/recover-password.component';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public submited: boolean;
  public form: FormGroup;
  constructor(public af: AFService, public activeModal: NgbActiveModal, public fb: FormBuilder, private modalService: NgbModal) {
    this.submited = false;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
      password: [null, Validators.minLength(7)],
    })
  }
  public login(form: any, event: Event) {
    this.submited = true;
    event.preventDefault();
    if (form.valid) {
      const user = { email: form.value.email, password: form.value.password }
      this.af.loginWithEmail(user.email, user.password).then(data => {
        this.activeModal.close()
      }).catch(err => {
        alert(err)
      })
    }
  }

  public signUp(): void {
    this.activeModal.close()
    this.modalService.open(SignupMemberComponent);
  }

  public recover(): void {
    this.activeModal.close()
    this.modalService.open(RecoverPasswordComponent);
  }

}
