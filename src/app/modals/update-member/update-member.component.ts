import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AFService } from '../../shared/services/firebase'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'update-member',
  templateUrl: './update-member.component.html',
  styleUrls: ['./update-member.component.css']
})
export class UpdateMemberComponent {
  public submited: boolean;
  public avatar: File;
  public form: FormGroup;
  constructor(public af: AFService, public activeModal: NgbActiveModal, public fb: FormBuilder) {
    this.submited = false;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
      username: [null, Validators.minLength(5)],
      currentPassword: [null, Validators.required],
      passwords: fb.group({
        password: [null, Validators.compose([Validators.minLength(7)])],
        confirm: [null, Validators.compose([Validators.minLength(7)])]
      }, { validator: this.areEqual })

    })
  }

  public update(form: any, event: Event, currentAvatar: File) {
    this.submited = true;
    event.preventDefault();
    if (form.valid) {
      const data = { currentPassword: form.value.currentPassword, avatar: currentAvatar, email: form.value.email, username: form.value.username, password: form.value.passwords.password }
      this.af.updateMember(data)
    }

  }

  public addAvatar(event: any, avatar) {
    const file = event.target.files[0]
    this.avatar = file;
  }
  private areEqual(group: any) {
    const password = group.controls.password.value
    const confirm = group.controls.confirm.value
    let areEqual: boolean;
    if (password == confirm)
      return null
    return {
      areEqual: false
    };
  }
}
