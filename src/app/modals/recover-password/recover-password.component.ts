import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AFService } from '../../shared/services/firebase'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent {
  public submited: boolean;
  public form: FormGroup;
  constructor(public af: AFService, public activeModal: NgbActiveModal, public fb: FormBuilder) {
    this.submited = false;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
    })
  }

  public recover(form: any, event: Event) {
    this.submited = true;
    event.preventDefault();
    if (form.valid) {
      const email = form.value.email
      this.af.recoverPassword(email).then(data => {
        alert(data)
        this.activeModal.close()
      }).catch(err => {
        alert(err)
      })
    }

  }

}
