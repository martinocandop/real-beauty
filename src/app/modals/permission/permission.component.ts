import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../shared/services/utils'

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent {

  constructor(public activeModal: NgbActiveModal, private us: UserService) { }
  public agree() {
    this.us.setPermission()
    this.activeModal.close()
  }
  public exit(){
    window.location.href = "https://www.google.com"
  }

}
