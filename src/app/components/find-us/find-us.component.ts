import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { APIService } from '../../shared/services/api'
@Component({
  selector: 'find-us',
  templateUrl: './find-us.component.html',
  styleUrls: ['./find-us.component.css']
})
export class FindUsComponent {
  public submited: boolean;
  public form: FormGroup;
  constructor(private AS: APIService, public fb: FormBuilder) {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
      name: [null, Validators.required],
      subject: [null, Validators.required],
      message: [null, Validators.required]
    })
  }

  public sendMessage(form: any, event: Event){
    this.submited = true;
    event.preventDefault();
    if (form.valid) {
      this.AS.sendMessage(form.value).then( () => {
        alert("DONE")
      }).catch( (err) => alert("ERROR"))
    }
  }

}
