import { Component, Input } from '@angular/core';
import { ModalService } from '../../shared/services/modal'
import { AFService } from '../../shared/services/firebase'
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  @Input() user: any;
  constructor(private ms: ModalService, public AF: AFService) {
  }

  public openSignup() {
    this.ms.openSignupMember()
  }
  public openLogin() {
    this.ms.openLogin()
  }
  public openUpdateMember() {
    this.ms.openUpdateMember()
  }
}
