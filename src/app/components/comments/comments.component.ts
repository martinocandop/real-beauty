import { Component, Input } from '@angular/core';
import { Observable } from "rxjs"
import { AFService } from '../../shared/services/firebase'

@Component({
  selector: 'comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent{
  @Input() comments: Observable<any>
  @Input() key: string;
  @Input() user: Observable<any>
  constructor(private AF: AFService) { }

  public send(message: string, userRef: string): void{
    this.AF.sendComment(this.key, message, userRef).then( data=> console.log(data)).catch(err=> console.log(err))
  }
}
