/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GirlCardComponent } from './girl-card.component';

describe('GirlCardComponent', () => {
  let component: GirlCardComponent;
  let fixture: ComponentFixture<GirlCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GirlCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GirlCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
