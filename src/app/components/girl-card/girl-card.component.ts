import { Component, OnInit, Input } from '@angular/core';
import { AssetsService } from '../../shared/services/utils'

@Component({
  selector: 'girl-card',
  templateUrl: './girl-card.component.html',
  styleUrls: ['./girl-card.component.css']
})
export class GirlCardComponent implements OnInit {
  @Input() girl: any;
  constructor(public AS: AssetsService) { }

  ngOnInit() {
  }

}
