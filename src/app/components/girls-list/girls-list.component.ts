import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'girls-list',
  templateUrl: './girls-list.component.html',
  styleUrls: ['./girls-list.component.css']
})
export class GirlsListComponent implements OnInit {
  @Input() girls: any;

  constructor() { }

  ngOnInit() {
  }

}
