import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { AssetsService } from '../../shared/services/utils'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public user: any;
  constructor(private route: ActivatedRoute, public AS: AssetsService) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['user'];
    this.user.avatar = this.user.avatar ? this.user.avatar : 'assets/pictures/escort-selfie.png'
    this.user.create_at = new Date(this.user.create_at).toUTCString().slice(0, 17)
    console.log(this.user.favorites)
  }

}
