import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { AssetsService } from '../../shared/services/utils'
import { AFService } from '../../shared/services/firebase'
import { Observable} from 'rxjs'

@Component({
  selector: 'app-escort',
  templateUrl: './escort.component.html',
  styleUrls: ['./escort.component.css']
})
export class EscortComponent implements OnInit {
  public escort: any;
  public popContent: string;
  public popOpen: boolean;
  public popOpen2: boolean;
  public user: any;
  public comments: Observable<any>
  constructor(private route: ActivatedRoute, private router: Router, public AS: AssetsService, public AF: AFService) {
    this.user = AF.user
  }

  ngOnInit() {
    this.escort = this.route.snapshot.data['escort'];
    this.escort.call = this.escort.calls.in ? 'In-call' : ''
    this.escort.call+= this.escort.calls.out && this.escort.calls.in ? '/' : ''
    this.escort.call+= this.escort.calls.out ? 'Out-call' : ''
    this.comments = this.AF.getComments(this.escort.$key)
    this.popOpen = false;
    this.popOpen2 = false;
  }

  public openPop(type: string): void{
    this.popOpen = type == this.popContent ? false : true
    this.popContent = type;

  }
  public openPop2(type: string): void{
    this.popOpen2 = type == this.popContent ? false : true
    this.popContent = type;

  }

  public chat(uid:string){
    this.AF.chats.take(1).subscribe( chats => {
      const exist = chats.filter( chat => {
          return chat.members.filter(member => member.$key == uid).length > 0 
      })
      if(exist.length > 0){
        this.router.navigate([`/chat/${exist[0].id}`]);

      }
      else{
        this.AF.createChat(uid).then( key =>  this.router.navigate([`/chat/${key}`])).catch( err => console.log(err))
      }
    })
  }
}
