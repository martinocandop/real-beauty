/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EscortComponent } from './escort.component';

describe('EscortComponent', () => {
  let component: EscortComponent;
  let fixture: ComponentFixture<EscortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
