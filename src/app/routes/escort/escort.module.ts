import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EscortRoutingModule } from './escort-routing.module';
import { EscortComponent } from './escort.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    EscortRoutingModule,
    SharedModule
  ],
  declarations: [EscortComponent]
})
export class EscortModule { }
