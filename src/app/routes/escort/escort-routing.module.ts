import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EscortComponent } from './escort.component'
import { EscortResolver } from'../../shared/services/escort-resolver'
const routes: Routes = [
  { path: 'escort/:uid', component: EscortComponent, resolve: {escort: EscortResolver}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class EscortRoutingModule { }
