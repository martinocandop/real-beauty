import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreationRoutingModule } from './creation-routing.module';
import { CreationComponent } from './creation.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CreationRoutingModule,
    SharedModule
  ],
  declarations: [CreationComponent]
})
export class CreationModule { }
