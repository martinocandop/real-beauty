import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AFService } from '../../shared/services/firebase'
import { Observable } from 'rxjs/Observable';
import { countries, services } from "./static"
import { AssetsService } from '../../shared/services/utils'

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import * as _ from "lodash";

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.css']
})
export class CreationComponent implements OnInit {
  public submited: boolean;
  public form: FormGroup;
  public modelCountry: any;
  public allCountries: any;
  public allServices: any;
  public ownServices: any;
  public selfie: any;
  public selfieUrl: string;
  public images: any;
  public imagesUrl: any;
  public days: String[]
  constructor(public af: AFService, public fb: FormBuilder, public AS: AssetsService) { 
    this.submited = false;
    this.allCountries = countries;
    this.allServices = services
    this.days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    this.images = [];
    this.ownServices = [];
    this.imagesUrl = [];
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const phoneRegex = /^[(]{0,1}[0-9]{2,3}[)]{0,1}[-\s\.]{0,1}[0-9]{1,5}[-\s\.]{0,1}[0-9]{1,7}[-\s\.]{0,1}[0-9]{1,7}[-\s\.]{0,1}[0-9]{1,7}$/
    this.form = fb.group({
      email: [null, Validators.pattern(emailRegex)],
      passwords: fb.group({
        password: [null, Validators.compose([Validators.required, Validators.minLength(7)])],
        confirm: [null, Validators.compose([Validators.required, Validators.minLength(7)])]
      }, { validator: this.areEqual }),
      name: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
      countries: fb.array([]),
      city: [null, Validators.minLength(4)],
      escort: fb.group({
        region: [null, Validators.minLength(4)],
        type: [null, Validators.minLength(4)],
        phone: [null, Validators.pattern(phoneRegex)],
        calls: fb.group({
          in: false,
          out: false
        }),
        about: [null, Validators.minLength(10)],
        age: null,
        origin: [null, Validators.minLength(5)],
        languages: fb.array([]),
        program: fb.group({
          monday: false,
          tuesday: "24",
          wednesday: "23",
          thursday: false,
          friday: "24",
          saturday: "20",
          sunday: false
        }),
        services: fb.array([]),
        hair: null,
        eyes: null,
        height: null,
        weight: null,
        breast: null,
        appointment: null,
        for: null,
        allowComments: null,
        vacations: false
      })
    })
  }

  public handleVacations(){
    let form  : any =  this.form.controls['escort']
    form.controls['vacations'].setValue(!form.controls['vacations'].value)
  }

  public handleDay(day: string, available?: boolean){
    const control : any = this.form.controls['escort']
    const value = control.controls['program'].controls[day.toLowerCase()].value
    switch(value){
      case "24":
          return available ? "off" : "on"
      case false:
        return available ? "on" : "off"
    }
  }

  public handleOther(day: string){
    const control : any = this.form.controls['escort']
    const value = control.controls['program'].controls[day.toLowerCase()].value
    if(typeof value != "boolean" && value != "24")
      return value
    else
      return ""
  }

  public addOtherInfo(input, day: string){
    const control : any = this.form.controls['escort']
    let value = input.target.value.trim()
    control.controls['program'].controls[day.toLowerCase()].setValue(input.target.value)
  }

  public addDayInfo(value: any, day: string){
    const control : any = this.form.controls['escort']
    control.controls['program'].controls[day.toLowerCase()].setValue(value)
  }

  public addLanguage(input: any):void{
    if (input.keyCode == 13) {
      const language = input.target.value.trim()
      const control : any = this.form.controls['escort']
      control.controls['languages'].push(this.fb.group([language, Validators.required]))
      input.target.value = ""
    }
  }

  public removeLanguage(index: number):void{
    const control : any = this.form.controls['escort']
    control.controls['languages'].controls.splice(index, 1)
  }

  private areEqual(group: any) {
    const password = group.controls.password.value
    const confirm = group.controls.confirm.value
    let areEqual: boolean;
    if (password == confirm)
      return null
    return {
      areEqual: false
    };
  }

  public register(form: any, event: Event) {
    this.submited = true;
    event.preventDefault();
    if(form.valid)
    if(form.value.escort.languages)
      form.value.escort.languages = form.value.escort.languages.map( language => {
        return language[0]
      })
    if(form.value.countries)
      form.value.countries = form.value.countries.map( country => {
        return country[0]
      })
    if(form.value.escort.services)
      form.value.escort.services = form.value.escort.services.map( service => {
        return service[0]
      })
    form.value.escort = _.omitBy(form.value.escort, _.isNil);
    console.log(form.value)
    this.af.registerEscort(
      form.value.name, 
      form.value.email, 
      form.value.passwords.password, 
      form.value.city,
      form.value.escort, 
      form.value.countries,
      this.selfie,
      this.images,
    ).then( data => {
      alert('Done')
    }).catch( err => alert(err))
  }

  public addCountry(event: any):void{
      const control : any = this.form.controls['countries']
      control.push(this.fb.group([event.item.name])) 
  }

  public removeCountry(index: number):void{
    const control : any = this.form.controls['countries']
    control.splice(index, 1)
  }

  public handleService(event, service){
    const checked = event.target.checked
    const control : any = this.form.controls['escort']
    let index : any = false
    service.active = checked    
    control.controls['services'].controls.forEach( (form, i) => {
      if(form.controls[0].value == service.name)
        index = i
    })
    if(index.toString() && !checked){
       control.controls['services'].controls.splice(index, 1)
    }
    
    else if(!index && checked)
      control.controls['services'].push(this.fb.group([service.name])) 
    
  }

  public addOwnService(input){
    const control : any = this.form.controls['escort']
    const value = input.target.value.trim()
    if (input.keyCode == 13) {
      if(value.length > 1){
        control.controls['services'].push(this.fb.group([value]))
        this.ownServices.push({active: true, name: value}) 
        input.value = ""
      }  
    }
  }
  public search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : countries.filter(v => new RegExp(term, 'gi').test(v.name)).slice(0, 10));

  public formatter = (x: {name: string}) => x.name;

  public addSelfie(event: any) {
    const file = event.target.files[0]
    const reader  = new FileReader();
    const self = this;
    this.selfie = file;

    reader.onloadend =  () =>{
      self.selfieUrl = reader.result;
    }

    if (file) 
      reader.readAsDataURL(file);
  }
  public addImages(event: any) {
    const files = event.target.files
    const self = this;
    for(let i = 0; i<files.length; i++){
      if (files[i]){
        const reader  = new FileReader();
        reader.readAsDataURL(files[i])
        this.images.push(files[i])
        reader.onloadend =  () =>{
          self.imagesUrl.push(reader.result);
        }
      }
    }

  }
  
  ngOnInit() {
  }

}
