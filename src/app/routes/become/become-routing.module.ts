import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BecomeComponent } from './become.component';

const routes: Routes = [
  { path: '', component: BecomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class BecomeRoutingModule { }
