import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BecomeRoutingModule } from './become-routing.module';
import { BecomeComponent } from './become.component';

@NgModule({
  imports: [
    CommonModule,
    BecomeRoutingModule
  ],
  declarations: [BecomeComponent]
})
export class BecomeModule { }
