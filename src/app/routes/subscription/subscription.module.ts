import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent } from './subscription.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SubscriptionRoutingModule,
    SharedModule
  ],
  declarations: [SubscriptionComponent]
})
export class SubscriptionModule { }
