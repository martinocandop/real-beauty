import { Component } from '@angular/core';
import { ModalService } from '../../shared/services/modal'

@Component({
  selector: 'subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent {

  constructor(private ms: ModalService) { }
  public openSignup() {
    this.ms.openSignupMember()
  }
  public openLogin() {
    this.ms.openLogin()
  }
}
