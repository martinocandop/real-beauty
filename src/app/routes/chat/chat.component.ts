import { Component, OnInit } from '@angular/core';
import { AFService } from '../../shared/services/firebase'
import { AssetsService } from '../../shared/services/utils'
import { Observable } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  public user: any;
  public active: any;
  public chats: Observable<any>
  constructor(public AF: AFService, public AS: AssetsService, private router: Router) { }

  ngOnInit() {
    this.AF.user.take(1).subscribe( user => {
      this.user = user;
      this.chats = this.AF.chats;
    })
    this.router.events.subscribe(e => {
      const id = e.url.split("/").pop()
      this.active = id;
    });
  }

  public changeChat(chat: any){
    this.router.navigate([`/chat/${chat.id}`]);
  }

  public handleAvatar(user):any{
    let img = 'assets/pictures/escort-selfie.png'
    if(user)
      img = user.avatar ? user.avatar : user.selfie 
    return this.AS.sanitizeStyle('url(' + img + ')')
  }

}
