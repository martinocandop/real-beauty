import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import { ConversationComponent } from './conversation/conversation.component';

const routes: Routes = [
  { path: 'chat', component: ChatComponent, 
    children: [
      {path: ":uid", component: ConversationComponent}
    ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ChatRoutingModule { }
