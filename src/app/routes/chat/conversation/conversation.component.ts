import { Component, OnInit } from '@angular/core';
import { AFService } from '../../../shared/services/firebase'
import { AssetsService } from '../../../shared/services/utils'
import { Observable } from 'rxjs'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css'],
})
export class ConversationComponent implements OnInit {
  public messages: Observable<any>
  public members: any;
  public id: any;
  public user: any;
  public message: string;
  constructor(private route: ActivatedRoute, private AF: AFService, public AS: AssetsService) {

    this.route.params
    .switchMap( params => {
      this.id = params['uid']
      return this.AF.getMessagesByRoomId(params['uid']).map(messages => messages)
    })
    .subscribe(messages => {
      this.messages = messages
    }, err => {
      console.log(err)
    });
   }

  ngOnInit() {
     
  }
  public handleAvatar(user):any{
    let img = 'assets/pictures/escort-selfie.png'
    if(user)
      img = user.avatar 
    return this.AS.sanitizeStyle('url(' + img + ')')
  }
  public sendMessage():void{
    this.AF.sendMessage(this.id, this.message, this.AF.getMyUserInfo().$key).then( data => {
      this.message = ""
    }).catch( err => console.log(err))
  }
  public userInfo(uid: string){
    if(!this.user)
      this.user = this.AF.getMyUserInfo()

    let info = this.AF.getUserMember(uid)
    return info;
  }
}
