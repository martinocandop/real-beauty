import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, BaseRequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs'

@Injectable()
export class HttpClient {
  constructor(private http: Http) {

  }

  public get(url, params): Observable<any> {
    let data = params ? this.toURLParams(params) : "";
    url = data != "" ? `${url}?${data}` : url
    return this.http.get(url).map(data =>
      data.json())
  }

  post(url, data) {
    return this.http.post(url, JSON.stringify(data)).map(data =>
      data.json())

  }

  delete(url, params) {
    return this.http.delete(url).map(data =>
      data.json())

  }

  toURLParams(data) {
    let url = Object.keys(data).map(function (k) {
      return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
    }).join('&')
    return url;
  }
}
