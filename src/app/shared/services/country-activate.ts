import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { UserService } from './utils';

@Injectable()
export class CountryActivate implements CanActivate {

  constructor(private router: Router, private US: UserService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(resolve => {
      if (this.US.getCountry()) {
        this.US.requestCountry().toPromise().then((data) => {
          console.log(data)
          this.US.setCountry(data.country)
          resolve(true)
        }).catch(() => {
          this.US.setCountry("CountryFailed")
          resolve(true)
        })
      }

    })
  }
}
