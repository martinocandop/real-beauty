import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AFService } from './firebase';

@Injectable()

export class EscortResolver implements Resolve<any> {
    constructor(private FS: AFService, private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): any {
        const uid = route.params['uid'];
        return new Observable(observer => {
            this.FS.getEscort(uid).then( escort => {
                observer.next(escort)
                observer.complete()
            }).catch( err => {
                Observable.empty()
                this.router.navigate(['/'])
            })
        });
    }
}