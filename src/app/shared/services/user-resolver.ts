import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AFService } from './firebase';

@Injectable()

export class UserResolver implements Resolve<any> {
    constructor(private FS: AFService, private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): any {
        const uid = route.params['uid'];
        return new Observable(observer => {
            this.FS.getUser(uid).subscribe( user => {
                observer.next(user)
                observer.complete()
            }, err => {
                Observable.empty()
                this.router.navigate(['/'])
            })
        });
    }
}