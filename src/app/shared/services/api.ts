
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs'
import { Http, Headers, RequestOptions, BaseRequestOptions } from '@angular/http';

@Injectable()

export class APIService {
    private options: any;
    constructor(private http: Http) {
        this.options = new BaseRequestOptions();
    }
    public sendMessage(form: any){
      return this.http.post("/API/message",
        { email: form.email,
          name: form.name,
          subject: form.subject,
          message: form.message
        }, this.options
      ).map(data=> data.json()).toPromise()
    }
}
