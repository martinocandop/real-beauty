import { Injectable, Inject } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PermissionComponent } from '../../modals/permission/permission.component';
import { SignupMemberComponent } from '../../modals/signup-member/signup-member.component';
import { UpdateMemberComponent } from '../../modals/update-member/update-member.component';

import { LoginComponent } from '../../modals/login/login.component';

@Injectable()
export class ModalService {
    constructor(private modalService: NgbModal) { }

    public openPermission() {
        const modalRef = this.modalService.open(PermissionComponent, { size: "lg", backdrop: 'static' });
    }

    public openSignupMember() {
        const modalRef = this.modalService.open(SignupMemberComponent);
    }

    public openUpdateMember() {
        const modalRef = this.modalService.open(UpdateMemberComponent);
    }
    
    public openLogin() {
        const modalRef = this.modalService.open(LoginComponent);
    }
}
