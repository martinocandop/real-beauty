
import { Injectable, Inject } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs'
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable, FirebaseAuthState } from 'angularfire2';
import * as firebase from 'firebase';
import { UserService } from './utils'
@Injectable()

export class AFService {
    private storage: any;
    public items: Observable<any>
    public escorts: any
    public user: Observable<any>
    public chats: Observable<any>
    public authState: FirebaseAuthState
    public users: Object;
    public myUserInfo: any;
    constructor(public af: AngularFire, private US: UserService) {
        this.users = {}
        this.user = this.af.auth.flatMap(auth => {
            if (auth != null) {
                this.authState = auth
                return this.af.database.object('/users/' + auth.uid).map( user => {
                    this.myUserInfo = user;
                    if(!this.chats)
                        this.chats = this.initializeChats(auth.uid)
                    return user;
                })
            }
            return Observable.of(auth)
        });
        this.escorts = this.US.requestCountry().flatMap(data => {
            return this.af.database.list('/disabled-countries', { query: { orderByChild: `countries/${data.country}`, equalTo: null } }).mergeMap(escorts => {
                if (escorts)
                    return Observable.combineLatest(
                        escorts.map(escort => this.af.database.object(`users/${escort.$key}`)),
                        (...values) => {
                            return escorts.map((escort, index) => {
                                return Object.assign({}, escort, {$key: escort.$key, city: values[index].city, selfie: values[index].avatar, name: values[index].name})
                            })
                        })
                else
                    return Observable.empty()

            })
        })

        this.storage = firebase.storage().ref()
    }
    public initializeChats(uid){
        return this.getRoomsByUser(uid).mergeMap(rooms => {
            if (rooms.length > 0){
                let members = []
                rooms.forEach(room => room.members.forEach(member => members.push(member)))
                return Observable.combineLatest(
                    members.filter(member => !this.getUserMember(member)).map(member => this.getUserInfo(member)),
                    (...values) => {
                        this.addUserMember(values)
                        let roomsData = rooms.map( room => {
                            room.members = room.members.map( member => {
                                return this.getUserMember(member)
                            })
                            return room
                        })
                        return roomsData
                    })        
            }
            else
                return Observable.of([])
        })
    }
    public loginWithEmail(email, password) {
        return new Promise((resolve, reject) => {
            this.af.auth.login({
                email: email,
                password: password,
            },
                {
                    provider: AuthProviders.Password,
                    method: AuthMethods.Password,
                }).then((user: FirebaseAuthState) => {
                    this.authState = user;
                    resolve(user)
                }).catch(err => {
                    reject(err.message)
                })
        })
    }

    public updateMember(data: any) {
        const user = data;
        let forkArr = []

        const info = new Promise((resolve, reject) => {
            if (user.email || user.password)
                this.authState.auth.reauthenticate(firebase.auth.EmailAuthProvider.credential(this.authState.auth.email, user.currentPassword)).then(data => {
                    if (user.email)
                        forkArr.push(this.authState.auth.updateEmail(user.email))
                    if (user.password)
                        forkArr.push(this.authState.auth.updatePassword(user.password))
                    resolve()
                })
            else
                resolve()
        })

        const username = new Promise((resolve, reject) => {
            if (user.username)
                this.evalUsername(user.username).then(() => {
                    forkArr.push(this.af.database.object('/users/' + this.authState.uid).set(Object.assign({}, this.getMyUserInfo(), { username: user.username })))
                    resolve()
                }).catch(() => reject())
            else
                resolve()
        })

        const avatar = new Promise((resolve, reject) => {
            if (user.avatar) {
                const self = this;
                const uploadTask = this.storage.child(`${this.authState.uid}/avatar.jpg`).put(user.avatar);
                uploadTask.on('state_changed', function (snapshot) {
                    console.log(snapshot)
                }, function (error) {
                    reject(error)
                }, function () {
                    const downloadURL = uploadTask.snapshot.downloadURL;
                    forkArr.push(self.af.database.object('/users/' + self.authState.uid).set(Object.assign({}, self.getMyUserInfo(), { avatar: downloadURL })))
                    resolve()
                });
            }
            else
                resolve()
        })

        return new Promise((resolve, reject) => {
            Promise.all([
                username,
                avatar,
                info
            ]).then(data => {
                Promise.all(forkArr).then(data=> {
                    resolve(data)
                }).catch(err => {
                    reject(err)
                })
            }).catch((err) => {
                reject(err)
            })
        })
    }
    public registerUser(email: string, password: string, username: string, type: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.evalUsername(username).then(() => {
                this.af.auth.createUser({
                    email: email,
                    password: password
                }).then(user => {
                    this.af.database.object('/users/' + user.uid).set({
                        create_at: Date.now(),
                        username: username,
                        type: type
                    }).then((user) => {
                        resolve("Account created")
                    })
                }).catch(err => {
                    reject(err.message)
                })
            }).catch(() => {
                alert("username taked")
            })
        })
    }
    public registerEscort(name:string, email: string, password: string, city: string, info: any, countries: any, selfie: any, images: any): Promise<string> {
        return new Promise((resolve, reject) => {
                this.af.auth.createUser({
                    email: email,
                    password: password
                }).then(user => {
                    this.handleImages(images, user.uid).then( imgs => {
                        if(imgs.length > 0){
                            info.images = imgs
                        }
                        let arr = [
                            this.af.database.object('/escorts/' + user.uid).set(info),
                            this.af.database.object('/disabled-countries/' + user.uid).set({countries: countries})
                        ]
                        if(selfie){
                            const self = this;
                            const uploadTask = this.storage.child(`${user.uid}/avatar.jpg`).put(selfie);
                            uploadTask.on('state_changed', function (snapshot) {
                                console.log(snapshot)
                            }, function (error) {
                                reject(error)
                            }, function () {
                                const downloadURL = uploadTask.snapshot.downloadURL;
                                arr.push(self.af.database.object('/users/' + user.uid).set({name: name, type: "escort", city: city, avatar: downloadURL, create_at: Date.now()}))
                                Promise.all(arr)
                                .then((user) => {
                                    resolve("Account created")
                                }).catch(err => reject(err.message))
                            });
                        }
                        else{
                            arr.push(this.af.database.object('/users/' + user.uid).set({name: name, city: city, type: "escort", create_at: Date.now()}))
                            Promise.all(arr)
                            .then((user) => {
                                resolve("Account created")
                            }).catch(err => reject(err.message))
                        }
                    }).catch(err=> reject(err))

                    
                }).catch(err => {
                    reject(err.message)
                })
        })
    }

    public handleImages(images, uid): Promise<any>{
        return new Promise ((resolve,reject) => {
            if(images.length > 0){
                images = images.map( image => {
                    return new Promise( (resolve,reject) => {
                            const self = this;
                            const uploadTask = this.storage.child(`${uid}/${Date.now()}.jpg`).put(image);
                            uploadTask.on('state_changed', function (snapshot) {
                                console.log(snapshot)
                            }, function (error) {
                                reject(error)
                            }, function () {
                                const downloadURL = uploadTask.snapshot.downloadURL;
                                resolve(downloadURL)
                            });
                    })
                })
                Promise.all(images).then( imgs => {
                    resolve(imgs)
                }).catch(err=> reject(err))
            }
            else
                resolve([])
           
        })
    }

    private evalUsername(username): Promise<any> {
        return new Promise((resolve, reject) => {
            const users = this.af.database.list('/users', { query: { orderByChild: 'username', equalTo: username } }).finally(() => {
                users.unsubscribe()
            }).subscribe(data => {
                if (data.length > 0)
                    reject()
                else
                    resolve()
            })
        })
    }
    public recoverPassword(email: string): Promise<string> {
        return new Promise((resolve, reject) => {

            firebase.auth().sendPasswordResetEmail(email).then(data => {
                resolve("Please check your email and change your password")
            }).catch(err => {
                resolve(err.message)
            })
        })

    }
    public changePassword(password: string): firebase.Promise<any> {
        return this.authState.auth.updatePassword(password)
    }
    public logout() {
        this.af.auth.logout()
    }

    public getEscort(uid:string): Promise<any>{
        return new Promise( (resolve, reject)=> {
            this.af.database.object(`/escorts/${uid}`).subscribe(escort => {
                this.af.database.object(`users/${escort.$key}`).subscribe( data => {
                    escort.selfie = data.avatar
                    escort.name = data.name
                    resolve(escort)
                })
            })
        })
        
    }

    public getUser(uid: string): Observable<any>{
        return this.af.database.object(`/users/${uid}`).mergeMap( user => {
            return this.af.database.list(`/favorites/${uid}`).mergeMap(escorts => {
                if (escorts)
                    return Observable.combineLatest(
                        escorts.map(escort => this.af.database.object(`users/${escort.$value}`)),
                        (...values) => {
                            user.favorites = escorts.map((escort, index) => {
                                return {$key: escort.$key, selfie: values[index].avatar, name: values[index].name}
                            })
                            return user;
                        })
                else
                    user.favorites = []
                    return Observable.of(user)

            })
        })
        

    }
    public getComments(uid: string): Observable<any>{
        return this.af.database.list(`comments/${uid}`, { query: { orderByChild: `create_at` } }).mergeMap(comments => {
                if (comments.length > 0)
                    return Observable.combineLatest(
                        comments.map(comment => this.af.database.object(`users/${comment.userRef}`)),
                        (...values) => {
                            return comments.map((comment, index) => {
                                return Object.assign({}, comment, { userRef: values[index], date: new Date(comment.create_at) })
                            }).reverse()
                        })
                else
                    return Observable.empty()

            })
    }

    public userInfo(uid: string):Observable<any>{
         return this.af.database.object(`/users/${uid}`)
    }
    public sendComment(uid: string, message: string, userRef: string): Promise<any>{
        return new Promise( (resolve, reject) => {
            this.af.database.list(`/comments/${uid}`).push({
                create_at: Date.now(),
                message: message,
                userRef: userRef
            }).then( (data) => resolve(data)).catch( (err) => reject(err))
        })
       
    }
    public sendMessage(uid: string, message: string, userRef: string): Promise<any>{
        return new Promise( (resolve, reject) => {
            this.af.database.list(`/messages/${uid}`).push({
                create_at: Date.now(),
                message: message,
                userRef: userRef
            }).then( (data) => resolve(data)).catch( (err) => reject(err))
        })
       
    }
    public getRoomsByUser(userID: string): Observable<any> {
        return this.af.database.list(`user-chats/${userID}`).mergeMap( rooms => {
            if (rooms.length > 0)
                return Observable.combineLatest(
                    rooms.map(room => this.af.database.object(`members/${room.$value}`)),
                    (...values) => {
                        values = values.map( value => value.filter(e => e != userID)).map( value => {
                            value.push(this.getMyUserInfo().$key)
                            return value
                        })
                        console.log(values)
                        return rooms.map((room, index) => {
                            return { id: room.$value, members: values[index] }
                        })
                    })
            else
                return Observable.of([])

        })

    }

    public createChat(friend: string): Promise<any>{
        return new Promise((resolve, reject) => {
            const chatId = Date.now()
            const members = [this.getMyUserInfo().$key, friend]
            members.forEach(member => {
                this.af.database.list(`user-chats/${member}`).push(chatId)
            })
            this.af.database.object(`members/${chatId}`).set(members)
            .then( () =>  resolve(chatId))
            .catch( err => reject(err))
        })
    }

    public getMessagesByRoomId(roomId: string): Observable<any> {
        return this.af.database.list(`messages/${roomId}`, { query: { orderByChild: `create_at` } })
    }

    public getMyUserInfo():any{
        return this.myUserInfo
    }
    public getUserInfo(uid: string): Observable<any>{
         return this.af.database.object(`/users/${uid}`)
    }
    public addUserMember(data):void{
        if(Array.isArray(data))
            data.forEach( user => {
                if(!this.users[user.$key])
                    Object.assign(this.users, {[user.$key]: user})
            })

        else
            Object.assign(this.users, {[data.$key]: data})

    }

    public getUserMember(uid: string): any{
        const user = this.users[uid]
        return user ? user : null
    }

}
