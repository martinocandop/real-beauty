import { Injectable } from '@angular/core';
import { HttpClient } from '../http-interceptor'
import { Observable } from 'rxjs'
@Injectable()

export class UserService {
    private permission: boolean;
    private country: string;

    constructor(private http: HttpClient){}

    public getPermission() {
        const permission = JSON.parse(localStorage.getItem("beauty-permission"))
        if (permission)
            return permission.agree
        return false


    }
    public setPermission() {
        localStorage.setItem("beauty-permission", JSON.stringify({ agree: true }))
    }

    public requestCountry() {
      return this.http.get('http://ip-api.com/json', '')

    }

    public getCountry(){
      return this.country
    }

    public setCountry(country:string){
      this.country = country
    }
}
