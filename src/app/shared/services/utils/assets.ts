import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl, SafeHtml, SafeResourceUrl } from '@angular/platform-browser';

@Injectable()

export class AssetsService {
    constructor(private sanitizer: DomSanitizer){}

    public sanitizeStyle(style:string): SafeUrl{
      return this.sanitizer.bypassSecurityTrustStyle(style);
    }
}
