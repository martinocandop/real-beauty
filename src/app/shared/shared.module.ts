import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AFService } from './services/firebase'

import { APIService } from './services/api'
import { ModalService } from './services/modal';
import { UserService, AssetsService } from './services/utils'
import { HttpClient } from './services/http-interceptor'
import { CountryActivate } from './services/country-activate'
import { EscortResolver } from './services/escort-resolver'
import { UserResolver } from'./services/user-resolver'
import { FindUsComponent } from '../components/find-us/find-us.component';
import { CommentsComponent } from '../components/comments/comments.component';
import { GirlCardComponent } from '../components/girl-card/girl-card.component';
import { GirlsListComponent } from '../components/girls-list/girls-list.component';

const MODULES = [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
];

const PIPES = [

];

const PROVIDERS = [
    HttpClient,
    AFService,
    APIService,
    ModalService,
    UserService,
    AssetsService,
    CountryActivate,
    EscortResolver,
    UserResolver
]

@NgModule({
    imports: [
        ...MODULES
    ],
    declarations: [
      ...PIPES,
      FindUsComponent,
      CommentsComponent,
      GirlCardComponent,
      GirlsListComponent
    ],
    exports: [
        ...MODULES,
        ...PIPES,
        FindUsComponent,
        CommentsComponent,
        GirlCardComponent,
        GirlsListComponent
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ...PROVIDERS
            ]
        };
    }
}
