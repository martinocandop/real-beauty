import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Observable } from 'rxjs'

import { AFService } from './shared/services/firebase'
import { UserService } from './shared/services/utils'
import { ModalService } from './shared/services/modal'

@Component({
  selector: 'beauty',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public items: Observable<any>
  public user: any;

  constructor(public af: AFService, private us: UserService, private ms: ModalService) {
    this.user = af.user
  }

  ngOnInit() {
    const permission = this.us.getPermission()
    if (!permission)
      this.ms.openPermission()
  }

}
