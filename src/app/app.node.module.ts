import { NgModule } from '@angular/core';
import { UniversalModule, isNode } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

//External Modules
import { AngularFireModule } from 'angularfire2';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

//Routes
import { appRoutes } from './app.routing';

//Main Component
import { AppComponent } from './index';

//Own Modules
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './routes/home/home.module';
import { EscortModule } from './routes/escort/escort.module';
import { UserModule } from './routes/user/user.module';
import { CreationModule } from './routes/creation/creation.module';
import { ChatModule } from './routes/chat/chat.module';

//Own Components
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { PermissionComponent } from './modals/permission/permission.component';
import { SignupMemberComponent } from './modals/signup-member/signup-member.component';
import { UpdateMemberComponent } from './modals/update-member/update-member.component';
import { LoginComponent } from './modals/login/login.component';
import { RecoverPasswordComponent } from './modals/recover-password/recover-password.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCYsws4zMPzcUnJnkDFcmB43H2mIlu393A",
  authDomain: "real-beauty.firebaseapp.com",
  databaseURL: "https://real-beauty.firebaseio.com",
  storageBucket: "real-beauty.appspot.com",
  messagingSenderId: "490492841794"
};

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterComponent,
    PermissionComponent,
    SignupMemberComponent,
    UpdateMemberComponent,
    LoginComponent,
    RecoverPasswordComponent
  ],
  imports: [
    UniversalModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    NgbModule.forRoot(),
    SharedModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    HomeModule,
    EscortModule,
    UserModule,
    CreationModule,
    ChatModule
  ],
  entryComponents: [
    PermissionComponent,
    SignupMemberComponent,
    UpdateMemberComponent,
    LoginComponent,
    RecoverPasswordComponent
  ]
})
export class AppModule {
}
