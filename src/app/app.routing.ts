import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'prefix' },
  { path: 'subscription', loadChildren: './routes/subscription/subscription.module#SubscriptionModule' },
  { path: 'become', loadChildren: './routes/become/become.module#BecomeModule' },

];
